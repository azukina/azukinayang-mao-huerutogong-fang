<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=divice-width,initial-scale=1">
<title>あずきなフェルト工房</title>
<link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet" media="screen and (min-width:768px)">
<link href="<?php echo get_template_directory_uri(); ?>/css/sp.css" rel="stylesheet" media="screen and (max-width:768px)">
<!--本体-->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.3.1.min.js" type="text/javascript"></script>	
<script src="<?php echo get_template_directory_uri(); ?>/js/script.js" type="text/javascript"></script>
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
</head>

<body>
<div class="wrap">
  <header>
    <div class="box">
      <p><a href="index.php"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="あずきなフェルト工房"/></a></p>
		<!--<nav>-->
        <ul>
          <li class="sp"><a href="#contents02">制作実績</a></li>
          <li class="sp"><a href="#contents03">制作の流れ</a></li>
          <li class="sp"><a href="#contents04">価格の目安</a></li>
          <li class="sp"><a href="#contents05">ご相談フォーム</a></li>
          <li class="ig"><a href="https://www.instagram.com/azusawa.kinako">Instagram</a></li>
        </ul>
		<!--</nav>-->
    </div>
  </header>
  
  <!--TOP-->
  <div class="contents">
    <h1><img src="<?php echo get_template_directory_uri(); ?>/images/top02.png" alt="世界に1つだけのオリジナルぬいぐるみをオーダーしてみませんか？"/></h1>
  </div>
  
  <!--制作実績-->
  <div id="contents02">
    <h2>制作実績</h2>
    <div class="pic">
      <div class="box">
        <p><img src="<?php echo get_template_directory_uri(); ?>/images/pic_02.png" class="border_radius" alt="実績画像01"/></p>
        <p>小林様依頼<br>
          「ペンタごん」</p>
      </div>
      <div class="box">
        <p><img src="<?php echo get_template_directory_uri(); ?>/images/pic_01.png" class="border_radius" alt="実績画像02"/></p>
        <p>テクノファンダ様依頼<br>
          「テクノファンダ」</p>
      </div>
      <div class="box">
        <p><img src="<?php echo get_template_directory_uri(); ?>/images/pic_03.png" class="border_radius" alt="実績画像03"/></p>
        <p>さっちゃん様依頼<br>
          「茶ムスター」</p>
      </div>
      <div class="box">
        <p><img src="<?php echo get_template_directory_uri(); ?>/images/pic_04.png" class="border_radius" alt="実績画像04"/></p>
        <p>ゆずっこ様依頼<br>
          「ゆずぽろ」</p>
      </div>
      <div class="box">
        <p><img src="<?php echo get_template_directory_uri(); ?>/images/pic_05.png" class="border_radius" alt="実績画像05"/></p>
        <p>ゆめのなか様依頼</p>
      </div>
      <div class="box">
        <p><img src="<?php echo get_template_directory_uri(); ?>/images/pic_06.png" class="border_radius" alt="実績画像06"/></p>
        <p>みらくる☆桃香様依頼<br>「パパロン」</p>
      </div>
    </div>
  </div>
  
  <!--制作の流れ-->
  <div id="contents03">
    <h2>制作の流れ</h2>
    <p class="h2u">ご入金から<span>約１ヶ月</span>で制作しております。<br>
      お急ぎの方はご相談下さい。</p>
    <div class="step6">
      <div class="box">
        <div class="step">
          <p>STEP<br>
            01</p>
          <div class="triangle"></div>
        </div>
        <div class="text">
          <p class="s_titlle">見積もり</p>
          <p>ご相談フォームにてお気軽にご相談ください。<br>
            下記価格目安を元にお見積りします。<br>
            <span>※1～2日程度</span></p>
        </div>
      </div>
      <div class="box">
        <div class="step">
          <p>STEP<br>
            02</p>
          <div class="triangle"></div>
        </div>
        <div class="text">
          <p class="s_titlle">お支払い</p>
          <p>お見積り金額を指定口座までお支払い下さい。</p>
        </div>
      </div>
      <div class="box">
        <div class="step">
          <p>STEP<br>
            03</p>
          <div class="triangle"></div>
        </div>
        <div class="text">
          <p class="s_titlle">制作</p>
          <p>入金確認後に制作に入ります。<br>
            <span>※3週間前後</span></p>
        </div>
      </div>
      <div class="box">
        <div class="step">
          <p>STEP<br>
            04</p>
          <div class="triangle"></div>
        </div>
        <div class="text">
          <p class="s_titlle">ご確認</p>
          <p>写真にてデザインの確認をして頂きます。<br>
            修正したい箇所がある場合はお申し付けください<br>
            <span>※1~2日程度</span> </p>
        </div>
      </div>
      <div class="box">
        <div class="step">
          <p>STEP<br>
            05</p>
          <div class="triangle"></div>
        </div>
        <div class="text">
          <p class="s_titlle">修正</p>
          <p>STEP04で頂いた内容を修正致します。<br>
            修正後は再度写真にてデザインをご確認頂けます。<br>
            <span>※1~2日程度</span></p>
        </div>
      </div>
      <div class="box">
        <div class="step">
          <p>STEP<br>
            06</p>
          <div class="triangle"></div>
        </div>
        <div class="text">
          <p class="s_titlle">発送</p>
          <p>ゆうパックにて発送致します。</p>
        </div>
      </div>
      <div class="box">
        <div class="step">
          <p>STEP<br>
            07</p>
        </div>
        <div class="text">
          <p class="s_titlle">到着</p>
          <p>お客様のお手元にオリジナルぬいぐるみが届きます。</p>
        </div>
      </div>
    </div>
  </div>
  
  <!--価格-->
  <div id="contents04">
    <h2>価格の目安</h2>
    <p class="h2u">あずきなフェルト工房では、<br>
      制作から発送までを１人で行っているため余計なコストがかからず、<br>
      他社ぬいぐるみ制作会社さんと比べお手頃の価格での制作を可能にしています。<br>
      「オリジナルキャラのぬいぐるみを１つだけ作って欲しい」<br>
      というお客様の希望を実現します。</p>
    <table>
      <tbody>
        <tr>
          <th>サイズ</th>
          <th>お値段</th>
        </tr>
        <tr>
          <td>Sサイズ<br>
            (約8cm)</td>
          <td>24,800円<span class="small">(+税)</span>～</td>
        </tr>
        <tr>
          <td>Mサイズ<br>
            (約15cm)</td>
          <td>29,800円<span class="small">(+税)</span>～</td>
        </tr>
        <tr>
          <td>Lサイズ<br>
            (約20cm)</td>
          <td>37,800円<span class="small">(+税)</span>～</td>
        </tr>
      </tbody>
    </table>
    <p class="small">プラス送料をご負担頂きます。<br>
      使用カラーが3色以上、形状が複雑なもの、お急ぎの場合などの場合、プラス料金が発生します。<br>
      上記サイズは目安のため、実際には１cm単位でサイズのご指定を頂けます。<br>
      制作が困難な形状や大きいサイズは制作をお断りする場合もございますので予めご了承下さい。<br>
      まずはお気軽にご相談フォームよりご質問ください。</p>
  </div>
  
  <!--お問い合わせ-->
  
  <div id="contents05"> 
    <!--    <h2>ご相談フォーム</h2>
-->
    <div class="contact">
      <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSeATfqtFj_Y031D8cdxEwK-_OTLGpnNZ0YFOwMhXEK4mHDP1Q/viewform?embedded=true" width="100%" height="1100" frameborder="0" marginheight="0" marginwidth="0">読み込んでいます...</iframe>
    </div>
  </div>
</div>
<footer> <small>© Copyright 2018 あずきなフェルト工房 All rights reserved.</small> </footer>
</body>
</html>
